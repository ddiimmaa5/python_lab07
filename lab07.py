# Запускати окремо
# Task1
import matplotlib.pyplot as plt
import numpy as np
import datetime

task1 = open('parabola.txt', 'a')
plt.title("Парабола")
plt.xlabel("Х")
plt.ylabel("У")
plt.grid()
x = int(input("Х: "))
y = int(input("Y: "))
a = int(input("Коефіцієнт a: "))
b = int(input("Коефіцієнт b: "))
c = int(input("Коефіцієнт с: "))
task1.write("Дата створення файлу: " + str(datetime.datetime.now()) + '\n')
task1.write("Формула: " + str(y) + "=" + str(a) + "*(" + str(x) + "^2)+" + str(b) + "*" + str(x) + "+" + str(c) + "\n")
task1.write(str(y) + " " + str(a) + " " + str(x) + " " + str(b) + " " + str(x) + " " + str(c) + "\n")
x = np.linspace(-6, 6, 15)
y = a * (x ** 2) + b * x + c
plt.plot(x, y)
plt.show()

# Task2
import matplotlib.pyplot as plt
import numpy as np
import datetime

task2 = []
f = open('parabola.txt', 'r')
k = 0
q = 0
for line in f:
    if k == 3:
        task2.append(line)
        k = 0
    k = k + 1
n1 = list(task2[0].split())
n2 = list(task2[1].split())
n3 = list(task2[2].split())
n4 = list(task2[3].split())
y = int(n1[0])
x = np.linspace(-10, 10)
graf, aks = plt.subplots()
y1 = int(n1[1]) * (x ** 2) + (int(n1[3]) * x) + int(n1[4])
y2 = int(n2[1]) * (x ** 2) + (int(n2[3]) * x) + int(n2[4])
y3 = int(n3[1]) * (x ** 2) + (int(n3[3]) * x) + int(n3[4])
y4 = int(n4[1]) * (x ** 2) + (int(n4[3]) * x) + int(n4[4])
plt.figure(figsize=(10, 10))
plt.subplot(4, 1, 1)
plt.plot(x, y1)
aks.plot(x, y1)
plt.title("Параболи:")
plt.ylabel("Y1", fontsize=14)
plt.grid(1)
plt.subplot(4, 1, 2)
plt.plot(x, y2)
plt.ylabel("Y2", fontsize=14)
plt.grid(1)
plt.subplot(4, 1, 3)
plt.plot(x, y3)
plt.ylabel("Y3", fontsize=14)
plt.grid(1)
plt.subplot(4, 1, 4)
plt.plot(x, y4)
plt.xlabel("X", fontsize=14)
plt.ylabel("Y4", fontsize=14)
plt.grid(1)
graf.savefig('Графік.png')
plt.show()
f.close()
# Task3
import matplotlib.pyplot as plt


def multiple_replace(target_str, replace_values):
    for i, j in replace_values.items():
        target_str = target_str.replace(i, j)
    return target_str


repl = {'"0,00"': '0.00', '"0,50"': '0.50', '"1,00"': '1.00', '"1,50"': '1.50', '"2,00"': '2.00', '"2,50"': '2.50', '"3,00"': '3.00', '"3,50"': '3.50', '"4,00"': '4.00', '"4,50"': '4.50', '"5,00"': '5.00', '"5,50"': '5.50', '"6,00"': '6.00', '"6,50"': '6.50', '"7,00"': '7.00', '"7,50"': '7.50', '"8,00"': '8.00', '"8,50"': '8.50', '"9,00"': '9.00', '"9,50"': '9.50', '"10,00"': '10.00'}
d_time = []
count = []
start_time = []
end_time = []
counts = []
y = [0]
student = int(input('Введіть номер студента: '))
j = 1
f = open('marks.lab6.csv', 'r')
for line in f:
    data = multiple_replace(line, repl).replace(',', ' ').split()
    start_time.append(data[4].replace(':', '.'))
    end_time.append(data[9].replace(':', '.'))
    count.append(float(data[15]))
    if student == j:
        for i in range(21):
            if i != 0:
                counts.append(float(data[-i]))
    j += 1
f.close()
for i in range(len(start_time)):
    d_time.append((float(end_time[i]) - float(start_time[i]))*100)
time = round(d_time[student - 1])
t = time * 60 / 20
x = [0, t, 2 * t, 3 * t, 4 * t, 5 * t, 6 * t, 7 * t, 8 * t, 9 * t, 10 * t, 11 * t, 12 * t, 13 * t, 14 * t, 15 * t, 16 * t, 17 * t, 18 * t, 19 * t, time * 60]
j = 1
counts.reverse()
y.append(counts[0])
while j < len(counts):
    if counts[j] > 0:
        y.append(y[j] + 0.5)
    else:
        y.append(y[j])
    j += 1
plt.title("Залежність підсумкової оцінки")
plt.xlabel("Час виконання")
plt.ylabel("Оцінка")
plt.grid()
plt.plot(x, y)
plt.show()
# Task4
import matplotlib.pyplot as plt


def multiple_replace(target_str, replace_values):
    for i, j in replace_values.items():
        target_str = target_str.replace(i, j)
    return target_str


repl = {'"0,00"': '0.00', '"0,50"': '0.50', '"1,00"': '1.00', '"1,50"': '1.50', '"2,00"': '2.00', '"2,50"': '2.50', '"3,00"': '3.00', '"3,50"': '3.50', '"4,00"': '4.00', '"4,50"': '4.50', '"5,00"': '5.00', '"5,50"': '5.50', '"6,00"': '6.00', '"6,50"': '6.50', '"7,00"': '7.00', '"7,50"': '7.50', '"8,00"': '8.00', '"8,50"': '8.50', '"9,00"': '9.00', '"9,50"': '9.50', '"10,00"': '10.00'}
count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
str_count = 0
f = open('marks.lab6.csv', 'r')
for line in f:
    data = multiple_replace(line, repl).replace(',', ' ').split()
    i = 1
    while i <= 20:
        if data[-i] == '0.50':
            count[i] += 1
        i += 1
    str_count += 1
f.close()
count.reverse()
count.pop()
str_count = str_count / 100
for i in range(len(count)):
    count[i] = count[i] / str_count
x = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"]
plt.title("Відсоток правильних відповідей")
plt.xlabel("Номер завдання")
plt.ylabel("Відсоток")
plt.bar(x, count)
plt.show()

# Task5
import matplotlib.pyplot as plt
import datetime
import time
import os, re

f = open('task5.txt', 'r', encoding='UTF-8')
st = f.read().lower()
f.close()
all_letters = len(st) - st.count(' ') - st.count('.') - st.count(',') - st.count('/') - st.count('!')
all_words = st.count(' ') + 1
print('Введіть слово для пошуку ')
print('Введіть букву для пошуку ')
t1 = time.perf_counter()
word = re.findall(r'\b{}\b'.format(input()), st)
letter = re.findall(r'{}'.format([str(i) for i in input().split()]), st)
t2 = time.perf_counter()
c_word = len(word)
c_letter = len(letter)
print('Частота слова в тексті: ', round(c_word / all_words, 1))
print('Частота літери в тексті: ', round(c_letter / all_letters, 1))
prod = float(c_word / (all_words / 100))
ses = 100 - prod
vals = [prod, ses]
labels = [word[0], "всі слова"]
fig, ax = plt.subplots()
ax.pie(vals, labels=labels)
ax.axis("equal")
if not os.path.isdir(r'task7'):
    os.mkdir(r'task7')
fig.savefig(r"task7/Cлова.png")
f1 = open("task7/answer.txt", 'a', encoding='UTF-8')
f1.write(f'Час пошуку {t2 - t1}\n')
f1.write("Дата створення першого графіку: " + '\n')
f1.write(str(datetime.datetime.now()) + '\n')
f1.write("змінні: " + str(c_word) + " " + str(prod) + " " + str(ses) + '\n')
plt.show()
prod = float(c_letter / (all_letters / 100))
ses = 100 - prod
vals = [prod, ses]
labels = [letter[0], "всі букви"]
fig, ax = plt.subplots()
ax.pie(vals, labels=labels)
ax.axis("equal")
fig.savefig(r"task7/Букви.png")
f1.write("Дата створення другого графіку: " + '\n')
f1.write(str(datetime.datetime.now()) + '\n')
f1.write("змінні: " + str(c_letter) + " " + str(prod) + " " + str(ses) + '\n')
plt.show()
f1.close()
# Task6
import matplotlib.pyplot as plt
import time


def d(n):
    t1 = time.perf_counter()
    task6 = []
    res = 0
    for i in range(n + 1):
        task6.append(i)
        res = i
    t2 = time.perf_counter()
    t = round(t2 - t1, 1)
    x = [0, t / 5, t / 4, t / 3, t / 2, t]
    y = [0, res / 5, res / 4, res / 3, res / 2, res]
    plt.plot(x, y)
    plt.xlabel("Час секунди")
    plt.ylabel("Кількість чисел")
    plt.show()
    print(f'Число {res}')


n = int(input("Ведіть число для знаходження простих чисел: "))
d(n)