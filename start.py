import matplotlib.pyplot as plt
import time


def d(n):
    t1 = time.perf_counter()
    task6 = []
    res = 0
    for i in range(n + 1):
        task6.append(i)
        res = i
    t2 = time.perf_counter()
    t = round(t2 - t1, 1)
    x = [0, t / 5, t / 4, t / 3, t / 2, t]
    y = [0, res / 5, res / 4, res / 3, res / 2, res]
    plt.plot(x, y)
    plt.xlabel("Час секунди")
    plt.ylabel("Кількість чисел")
    plt.show()
    print(f'Число {res}')


n = int(input("Ведіть число для знаходження простих чисел: "))
d(n)